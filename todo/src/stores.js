import { writable } from 'svelte/store'

export const message = writable('')

message.subscribe(value => {
  if (!value) return
  const timeout = setTimeout(() => {
    message.set('')
    clearTimeout(timeout)
  }, 1000)
})
