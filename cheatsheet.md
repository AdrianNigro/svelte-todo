# Svelte-todo demo cheatsheet

## Preparativos

1. Abrir links

1. deployar todo.bak

```bash
$ cd todo.bak
$ time now
```

## Mostrar presentación

[https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/](https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/)

## Demo

### 1. Create todo svelte app

```bash
$ cd /home/sas/devel/apps/svelte/svelte-todo
$ npx degit sveltejs/template todo
$ cd todo
$ npm install
$ npm run dev
```

1. Mostrar la aplicación

- package.json
- rollup.conf.js
- src/main.js
- src/App.svelte
- public/bundle.js

### 2. Crear la aplicación

1. `01-create_App-svelte: Create App.svelte calling Todos.svelte component`

1. Crear components/Todos.svelte

```html
<script>
  let todos = []
</script>
```

`02-todo_container-html: Create html for todo-container`

1. Mostrar la app

1. Copiar los estilos (mencionar tailwindcss)

```bash
$ ../scripts/cp_css.sh
$ npm run dev
```

### 3. Contadores

`03-create_counters-js: Create variables for todos counters`

`04-counter_h3-html: Display todos counters`

### 4. Bucle

`05-create_todos-js: Create todos array`

Mostrar como actualiza

Crear un bucle

```html
    <div class="items">
      <ul>
      {#each todos as todo, index (todo.id)}
        <li>{index}. {todo.text} ({todo.id})</li>
      {/each}
      </ul>
    </div>
```

### 5. Props

Declarar todos como una prop y pasarla desde app

### 6. Items - toggle & removeTodo

`06-display_items-html: Display items`

```html
<button on:click={() => t.done = !t.done} class="{t.done ? 'pending' : 'done'}">{t.done ? 'Not done' : 'Done'}</button>
<button on:click={() => removeTodo(t)} class="remove">Remove</button>
```

`07-removeTodo_function-js: Create remove todo function`

Usar expresiones reactivas para actualizar los contadores automáticamente

```html
  $: totalTodos = todos.length
  $: totalDone = todos.filter(t => t.done).length
  $: totalPending = totalTodos - totalDone
```

### 7. addTodo

`08-add_todo-html: Create div for add-todo`

1. declarar `let newTodo = ''`

2. mostrar el bind manual: `<input value={newTodo} on:keydown={(e) => newTodo = e.target.value}`

3. mostrar `bind:value`

4. mostrar: `todos.push` no es reactivo

`09-addTodo_function-js: Create addTodo function`

`10-calculate_next_id-js: Reactively calculate nextId`

### 8. Filtros

`11-filter-function-js: Create reactive filter function`

`12-filter_div-html: Create html for showing filters`

### 9. Crear un componente

creamos componentes/Todo.svelte

`13-create_todo_component-svelte: Create Todo component`

Importar Todo.svelte en Todos.svelte

```js
 import Todo from './Todo.svelte'
```

Usar <Todo todo={todo}>

`14-update_removeTodo-js: Update removeTodo function to get todo from event`

### 10. Stores

`15-create_store-js: Create store.js`

Creamos ./components/Message.svelte

`16-create_Message_component-svelte: Create svelte Message component`

Agregamos Message.svelte a App.svelte

```html
<script>
	import Message from './components/Message.svelte'
	import Todos from './components/Todos.svelte'
	[...]
</script>

<Message />
<Todos {todos}/>
```

Importamos el store en Todos.svelte y hacemos una prueba

```html
<script>
  import { message } from '../stores'
  import Todo from './Todo.svelte'
  [...]
  $message = 'hola!!!'
```

`17-update_removeTodo_and_addTodo_to_use_store: Update removeTodo and addTodo to use the message store`

`18-add_timeout_to_message_store: 18-add_timeout_to_message_store`
