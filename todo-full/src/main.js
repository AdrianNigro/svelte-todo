import App from './v2/components/App.svelte';
import "./main.css";

const app = new App({
	target: document.body
});

export default app;