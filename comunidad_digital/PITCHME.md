<!-- ---?image=assets/dr_js/img01.jpg -->

@snap[north text-06 span-100]
## Nuevas tendencias en el front-end
##### un caso práctico con
@snapend

@snap[midpoint fragment]
# Svelte
@snapend

---

@snap[north span-50]
### Acerca de mí...
@snapend

@snap[west span-33]
![IMAGE](assets/img/opensas_avatar.jpg)
@snapend

@snap[east span-67]
@size[0.7em](- Ingeniero de la UTN <br>)
@size[0.7em](- Arquitecto de Software en el ex-Ministerio de Trabajo <br>)
@size[0.7em](- Defensor del software libre y otras libertades <br>)
@size[0.7em](- Participo en la comunidad datera y nerd)
@snapend

@snap[south]
twitter: [@opensas](https://twitter.com/opensas)
@snapend

---

@snap[north text-06 span-100]
## 30 años de desarrollo web en 2 slides...
@snapend

@snap[text-08 fragment]

<ul>

<li>1990 - 1995: surgen HTML, CSS and JavaScript, los pilares de la programación web.</li>

<li>1996 - 2004: Las páginas son mayormente estáticas, cada interacción supone cargar de vuelta la página.</li>

<li>2005: jQuery se instala como la librería dominante en desarrollo web (api más amigable, salva las inconsistencias entre los browsers)</li>

<li>2006: Google presenta Google Maps, la primera aplicación AJAX, usando una olvidada tecnología de 1999 (XMLHttpRequest)</li>

</ul>

@snapend

---

@snap[north text-06 span-100]
## 30 años de desarrollo web en 2 slides...
@snapend

@snap[text-08 fragment]

<ul>

<li>2010 Backbone: la primera librería orientada a crear Single Page Applications (SPA)</li>

<li>2010 AngularJS: el primer framework que provee una arquitectura completa para el desarrollo front-end. Trajo data binding bi-directional, inyección de dependencias y la posibilidad de crear componentes reutilizables</li>

<li>2013 React: basado en la idea de componentes y el Virtual DOM, tomando elementos de la programación funcional (state driven apps, enfoque declarativo, sin mutationces)</li>

<li>2014 Vue: en algunos aspectos es un punto intermedio entre Angular y React, con una curva de parendizaje más liviana.</li>

</ul>

@snapend
---

@snap[north-west text-06 span-100]
### State of JavaScript 2018 - Overview
@snapend

@snap[midpoint text-08 span-100 fragment img-state-of-js-overview]
![Overview](assets/img/state_of_js_overview.png)
@snapend


@snap[south-east text-04 span-100]
source: [The State of JavaScript Survey: Front-end Frameworks - Overview](https://2018.stateofjs.com/front-end-frameworks/overview/)
@snapend

---

@snap[north-west text-06 span-100]
### State of JavaScript 2018 - Other libraries
@snapend

@snap[midpoint text-08 span-100 fragment img-state-of-js-other-libraries]
![Other_libraries](assets/img/state_of_js_other_libraries.png)
@snapend

@snap[south-east text-04 span-100]
source: [The State of JavaScript Survey: Front-end Frameworks - Other libraries](https://2018.stateofjs.com/front-end-frameworks/other-libraries/)
@snapend

---

@snap[north-west text-06 span-100]
### State of JavaScript 2018 - Conclusion
@snapend

@snap[midpoint text-08 span-100 fragment img-state-of-js-svelte]
![Svelte](assets/img/state_of_js_conclusion_svelte.png)
@snapend

@snap[south-east text-04 span-100]
source: [The State of JavaScript Survey: Front-end Frameworks - Conclusion](https://2018.stateofjs.com/front-end-frameworks/conclusion/)
@snapend

---

@snap[north]
# Svelte!
@snapend

@snap[west snap-100 fragment]
### Ni librería...
@snapend

@snap[east snap-100 fragment]
### Ni framework...
@snapend

@snap[south fragment]
# Compilador!!!
@snapend

+++

@snap[north-west text-06 span-100]
### Infaltable meme gratuito...
@snapend

@snap[midpoint text-08 span-100]
![SIZE](assets/img/svelte_react_meme.jpg)
@snapend

---

@snap[north text-06 span-100]
## Problemas actuales del<br /> desarrollo de aplicaciones web:
@snapend

@snap[text-10 fragment]

<ul>

<li>Complejidad: <strong>MUY difícil</strong> (demasiadas cosas para aprender)</li>

<li>Tamaño: <strong>MUY pesado</strong> (demasiados kb para descargar, interpretar y ejecutar)</li>

<li>Verborrágico: <strong>MUCHO código</strong> (demasiado código para escribir y mantener)</li>

</ul>

@snapend

---

@snap[north span-100]

### La filosofía de Svelte...
@fa[quote-left quote-graphql text-white](Hacer más con menos)

@snapend

@snap[midpoint span-60]
![SVELTE](assets/img/svelte.png)
@snapend

@snap[south-west span-30 text-08 fragment]
@box[rounded text-white box-graphql](Menos código...#para el usuario<br>&nbsp;)
@snapend

@snap[south span-30 text-08 fragment]
@box[rounded text-white box-graphql](Menos código...#para el desarrollador)
@snapend

@snap[south-east span-30 text-08 fragment]
@box[rounded text-white box-graphql](Menos código...#para ejecutar<br>&nbsp;)
@snapend

---

@snap[north-west text-06 span-100]
### Menos código para el usuario... (size matters)
@snapend

@snap[midpoint text-08 span-100 fragment img-svelte-compared-size]
![SIZE](assets/img/svelte_compared_size.png)
@snapend

@snap[south-east text-04 span-100]
source: [A RealWorld Comparison of Front-End Frameworks](https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/)
@snapend

---

@snap[north-west text-06 span-100]
### Menos código para el dev... (happiness matters)
@snapend

@snap[midpoint text-08 span-100 fragment img-svelte-compared-loc]
![SIZE](assets/img/svelte_compared_loc.png)
@snapend

@snap[south-east text-04 span-100]
source: [A RealWorld Comparison of Front-End Frameworks](https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/)
@snapend

---

@snap[north-west text-05 span-80]
### Menos código para el dev... (happiness matters)
@snapend

@snap[north-east text-04 span-20]
#### [working example](https://4bvtg.codesandbox.io/)
@snapend

@snap[west fragment]
![REACT](assets/img/loc/react_loc.png)
@snapend

@snap[midpoint fragment]
![VUE](assets/img/loc/vue_loc.png)
@snapend

@snap[east fragment]
![SVELTE](assets/img/loc/svelte_loc.png)
@snapend

@snap[south-east text-04 span-100 fragment]
[React example](https://codesandbox.io/s/lesscode-sveltevsreeact-ujt7h) |
[Vue example](https://codesandbox.io/s/lesscode-sveltevsvue-sok3i) |
[Svelte example](https://codesandbox.io/s/lesscode-svelte-4bvtg)
@snapend

---

@snap[north-west text-10 span-100]
### Menos código para el dev
@snapend

@snap[midpoint text-07 span-100]
<blockquote>
<p>Death to boilerplate... How?</p>

<p>
Because Svelte is a compiler, we're not bound to the peculiarities of JavaScript: we can design a component authoring experience, rather than having to fit it around the semantics of the language. Paradoxically, this results in more idiomatic code — for example using variables naturally rather than via proxies or hooks — while delivering significantly more performant apps.
</p>
</blockquote>
@snapend

@snap[south-east text-04 span-100]
Source: [Death to boilerplate](https://svelte.dev/blog/write-less-code#Death_to_boilerplate)
@snapend
---

@snap[north-west text-10 span-100]
### Menos código para ejecutar
##### [Virtual DOM is pure overhead](https://rethinking-reactivity.surge.sh/#slide=9)
@snapend

@snap[midpoint text-05 span-100]
<blockquote>
<p>Why do frameworks use the virtual DOM then?</p>

<p>It's important to understand that virtual DOM isn't a feature. It's a means to an end, the end being declarative, state-driven UI development. Virtual DOM is valuable because it allows you to build apps without thinking about state transitions, with performance that is generally good enough.</p>

<p>But it turns out that we can achieve a similar programming model without using virtual DOM — and that's where Svelte comes in.</p>
</blockquote>
@snapend

@snap[south-east text-04 span-100]
Source: [Virtual DOM is pure overhead](https://svelte.dev/blog/virtual-dom-is-pure-overhead#Why_do_frameworks_use_the_virtual_DOM_then)
@snapend

---

## Reactividad en serio

- [Rich Harris - Rethinking reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao) | [presentation](https://rethinking-reactivity.surge.sh)

- Mike Boston's [observable](https://rethinking-reactivity.surge.sh/#slide=19)

- [Destiny operator](https://rethinking-reactivity.surge.sh/#slide=20)

---

#### Cómo le decimos a la computadora<br> que un valor cambió

##### <br>Con una api...

```javascript
const { count } = this.state;
this.setState({
  count: count + 1
});
```

---

#### Cómo le decimos a la computadora<br> que un valor cambió

##### <br>O mejor sin ninguna api..

```javascript
count += 1;
```

##### <br>...y que el compilador se encargue de "instrumentarlo"

```javascript
count += 1; $$invalidate('count', count);
```

---

@snap[north text-10 span-100]
## A codear!
@snapend

@snap[midpoint span-100]
![SIZE](assets/img/enough_talk.jpg)
@snapend

@snap[south-east text-04 span-20]
#### [Ver app online](https://todo.opensas.now.sh/)
@snapend

---


@snap[north span-100]
### La verdad acerca de Svelte...
@snapend

@snap[west span-30 text-10 fragment]
Ni librería
@snapend

@snap[midpoint span-30 text-10 fragment]
Ni framework
@snapend

@snap[east span-40 text-10 fragment]
Ni compilador!!!
@snapend

@snap[south span-100 text-20 fragment]
Lenguaje!
@snapend

@snap[south-west text-04 span-100]
source: [SvelteScript y Evan You](https://rethinking-reactivity.surge.sh/#slide=22)
@snapend

@snap[south-east text-04 span-100]
source: [What is Svelte](https://gist.github.com/Rich-Harris/0f910048478c2a6505d1c32185b61934)
@snapend

---

#### Svelte: un lenguaje para definir interfaces reactivas

- Extiende HTML para usar JS en el markup, responder a eventos, y manejar condiciones, bucles y código asincrónico

- Extiende CSS con un mecanismo de scopes que evita que los estilos se pisen entre sí

@snap[south-east text-04 span-100]
source: [What is Svelte](https://gist.github.com/Rich-Harris/0f910048478c2a6505d1c32185b61934)
@snapend

---

#### Svelte: un lenguaje para definir interfaces reactivas

- Extiende JavaScript transformándolo en un lenguaje reactivo, sin romper el lenguaje ni el tooling existente

- Instrumenta las asignaciones a variables y propiedades para hacerlas reactivas

- Agrega la sentencia '$:' para ejecutar comandos cuando cambian las variables a las que hace referencia

@snap[south-east text-04 span-100]
source: [What is Svelte](https://gist.github.com/Rich-Harris/0f910048478c2a6505d1c32185b61934)
@snapend

---

#### Svelte: Cómo seguir?

- Ver la presentación [Rethinking Reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao)

- Seguir el [tutorial interactivo](https://svelte.dev/tutorial/basics)

- Comprar el [curso en Udemy](https://www.udemy.com/sveltejs-the-complete-guide/?couponCode=EARLY_YT) ($9.99!!!)

- Ver [Sapper](https://sapper.svelte.dev/): The next small thing in web development

- Ver [Svelte Native](https://svelte-native.technology/): The Svelte Mobile Development Experience

---?image=assets/images/gitpitch-audience.jpg

### ¿Preguntas?

<br>

@fa[twitter gp-contact]&nbsp;[@opensas](https://twitter.com/opensas)

@fa[github gp-contact]&nbsp;[opensas](https://github.com/opensas)

@fa[gitlab gp-contact]&nbsp;[opensas](https://gitlab.com/opensas/svelte-todo)

@fa[chalkboard-teacher gp-contact]&nbsp;[slides](https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/)

@fa[list-alt gp-contact]&nbsp;[application on line](https://todo.opensas.now.sh/)

---

#### Agradecimientos

### A todos ustedes!

<br>

@fa[twitter gp-contact]&nbsp;[@Rich_Harris](https://twitter.com/Rich_Harris) - Rich Harris

@fa[twitter gp-contact]&nbsp;[@SvelteJs](https://twitter.com/SvelteJs) - La comunidad Svelte

@fa[twitter gp-contact]&nbsp;[@gitpitch](https://twitter.com/gitpitch) - David Rusell

@fa[twitter gp-contact]&nbsp;[@bastianhell](https://twitter.com/bastianhell) - Sebastian Ghelerman

@fa[twitter gp-contact]&nbsp;[@ModernizacionAR](https://twitter.com/ModernizacionAR) - ModernizaciónAR
