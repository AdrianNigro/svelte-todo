<!-- ---?image=assets/dr_js/img01.jpg -->

@snap[north]
# Svelte
@snapend

@snap[west span-50 text-center fragment]
### @fa[minus-circle text-orange] Library
@snapend

@snap[east span-50 text-center fragment]
### @fa[minus-circle text-orange] Framework
@snapend

@snap[south span-100 fragment]
## It's a Compiler!!!
@snapend

---

@snap[north span-50]
### About Me...
@snapend

@snap[west shadow span-33]
![IMAGE](assets/img/opensas_avatar.jpg)
@snapend

@snap[east span-60]
@ul[list-square-bullets list-spaced-bullets text-07 ](false)
- Engineer at UTN
- Developed systems at the ex-Ministry of Labor
- Supporter of free software and other freedoms
- Enjoy participating in the data and nerd communities
@ulend
@snapend

@snap[south]
twitter: [@opensas](https://twitter.com/opensas)
@snapend

---

@snap[north span-100]
### About Rich Harris...
@snapend

@snap[west shadow span-33]
![IMAGE](assets/img/rich_harris_avatar.jpg)
@snapend

@snap[east span-60]
@ul[list-square-bullets list-spaced-bullets text-07 ](false)
- Background...philosopher!
- Investigative journalist at The Guardian and the New York Times
- Creator of SvelteJs, Ractive y Rollup
- Conference Speaker
@ulend
@snapend

@snap[south]
twitter: [@rich_harris](https://twitter.com/rich_harris)
@snapend

---

@snap[north span-100]

### The Svelte Philosophy
@fa[quote-left quote-graphql](Do More With Less)

@snapend

@snap[midpoint shadow span-60]
![SVELTE](assets/img/svelte.png)
@snapend

@snap[south-west span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](Less Code#for the user)
@snapend

@snap[south span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](Less Code#for the developer)
@snapend

@snap[south-east span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](Less Code#to execute)
@snapend

---

@snap[north text-06 span-100]
### Less Code for the User... (size matters)
@snapend

@snap[south span-70]
![SIZE](assets/img/svelte_compared_size.png)
@snapend

@snap[south text-04 span-100]
source: [A RealWorld Comparison of Front-End Frameworks](https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/)
@snapend

---

@snap[north text-06 span-100]
### Less Code for Developers... (happiness matters)
@snapend

@snap[south span-70 fragment]
![SIZE](assets/img/svelte_compared_loc.png)
@snapend

@snap[south text-04 span-100]
source: [A RealWorld Comparison of Front-End Frameworks](https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/)
@snapend

---

@snap[north text-05 span-80]
### Less Code for Developers... (happiness matters)
@snapend

@snap[south-west text-04 span-50]
#### [working example](https://4bvtg.codesandbox.io/)
@snapend

@snap[west shadow fragment]
![REACT](assets/img/loc/react_loc.png)
@snapend

@snap[midpoint shadow fragment]
![VUE](assets/img/loc/vue_loc.png)
@snapend

@snap[east shadow fragment]
![SVELTE](assets/img/loc/svelte_loc.png)
@snapend

@snap[south-east text-04 span-100 fragment]
[React example](https://codesandbox.io/s/lesscode-sveltevsreeact-ujt7h) |
[Vue example](https://codesandbox.io/s/lesscode-sveltevsvue-sok3i) |
[Svelte example](https://codesandbox.io/s/lesscode-svelte-4bvtg)
@snapend

---

@snap[north span-100]
### Less Code for Developers
@snapend

@snap[midpoint text-07 span-80]
<blockquote>
<p>Death to boilerplate... How?</p>

<p>
Because Svelte is a compiler, we're not bound to the peculiarities of JavaScript: we can design a component authoring experience, rather than having to fit it around the semantics of the language. Paradoxically, this results in more idiomatic code — for example using variables naturally rather than via proxies or hooks — while delivering significantly more performant apps.
</p>
</blockquote>
@snapend

@snap[south text-04 span-100]
Source: [Death to boilerplate](https://svelte.dev/blog/write-less-code#Death_to_boilerplate)
@snapend
---

@snap[north span-100]
### Less Code to Execute
##### [Virtual DOM is pure overhead](https://rethinking-reactivity.surge.sh/#slide=9)
@snapend

@snap[midpoint text-06 span-80]
<br>
<blockquote>
<p>Why do frameworks use the virtual DOM then?</p>

<p>It's important to understand that virtual DOM isn't a feature. It's a means to an end, the end being declarative, state-driven UI development. Virtual DOM is valuable because it allows you to build apps without thinking about state transitions, with performance that is generally good enough.</p>

<p>But it turns out that we can achieve a similar programming model without using virtual DOM — and that's where Svelte comes in.</p>
</blockquote>
@snapend

@snap[south text-04 span-100]
Source: [Virtual DOM is pure overhead](https://svelte.dev/blog/virtual-dom-is-pure-overhead#Why_do_frameworks_use_the_virtual_DOM_then)
@snapend

---
@snap[north span-100]
### Serious Reactivity
@snapend

- [Rich Harris - Rethinking reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao) | [presentation](https://rethinking-reactivity.surge.sh)

- Mike Boston's [observable](https://rethinking-reactivity.surge.sh/#slide=19)

- [Destiny operator](https://rethinking-reactivity.surge.sh/#slide=20)

---

@snap[north span-100 text-09]
#### How to tell the computer that some state has changed?
##### With an API...
@snapend

```javascript
const { count } = this.state;
this.setState({
  count: count + 1
});
```

---

@snap[north span-100 text-09]
#### How to tell a computer that some state has changed?
##### Or better yet, without any API...
@snapend

```javascript
count += 1;
```

<br>
#### Since we're a compiler,<br>we can do that using "instrumentation"
<br>

```javascript
count += 1; $$invalidate('count', count);
```

---

@snap[north span-100]
## Lets Code!
@snapend

@snap[midpoint shadow span-100]
![SIZE](assets/img/enough_talk.jpg)
@snapend

@snap[south-east text-04 span-20]
#### [Ver app online](https://todo.opensas.now.sh/)
@snapend

---


@snap[north span-100]
### The Truth about Svelte
@snapend

@snap[west span-20 text-center fragment]
Not a Library
@snapend

@snap[midpoint span-20 text-center fragment]
Not a Framework
@snapend

@snap[east span-20 text-center fragment]
Not a Compiler
@snapend

@snap[south span-100 fragment]
#### It's a Language!
@snapend

@snap[south-west text-04 span-100]
source: [SvelteScript y Evan You](https://rethinking-reactivity.surge.sh/#slide=22)
@snapend

@snap[south-east text-04 span-100]
source: [What is Svelte](https://gist.github.com/Rich-Harris/0f910048478c2a6505d1c32185b61934)
@snapend

---

@snap[north span-100]
#### Svelte: a language for reactive interfaces
@snapend

@snap[south span-100]
@ul[list-spaced-bullets](false)
- Extend JavaScript transforming it into a reactive language, without breaking the language or existing tooling
- Instrument variable assignment and properties to make them **reactive**
- Add the '$:' statement to execute commands when the variables referenced change
@ulend
@snapend

@snap[south text-04 span-100]
source: [What is Svelte](https://gist.github.com/Rich-Harris/0f910048478c2a6505d1c32185b61934)
@snapend

---

@snap[north span-100]
#### Svelte: Learning Path
@snapend

- Watch the [Rethinking Reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao) presentation
- Follow the [Svelte Interactive Tutorial](https://svelte.dev/tutorial/basics)
- Buy the [Svelte Udemy Course](https://www.udemy.com/sveltejs-the-complete-guide/?couponCode=EARLY_YT) ($9.99!!!)
- See the [Sapper Framework](https://sapper.svelte.dev/)<br>The next small thing in web development
- See [Svelte Native](https://svelte-native.technology/)<br>The Svelte Mobile Development Experience

---?image=assets/images/gitpitch-audience.jpg

@snap[north span-100 h3-white]
### Questions?
@snapend

@fa[twitter text-white]&nbsp;[@opensas](https://twitter.com/opensas)

@fa[github text-white]&nbsp;[opensas](https://github.com/opensas)

@fa[gitlab text-white]&nbsp;[opensas](https://gitlab.com/opensas/svelte-todo)

@fa[chalkboard-teacher text-white]&nbsp;[slides](https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/)

@fa[list-alt text-white]&nbsp;[online demo app](https://todo.opensas.now.sh/)

---

### Thanks Everyone!

<br>

@fa[twitter]&nbsp;[@Rich_Harris](https://twitter.com/Rich_Harris) - Rich Harris

@fa[twitter]&nbsp;[@SvelteJs](https://twitter.com/SvelteJs) - The Svelte Community

@fa[twitter]&nbsp;[@tailwindcss](https://twitter.com/tailwindcss) - tailwindcss

@fa[twitter gp-contact]&nbsp;[@gitpitch](https://twitter.com/gitpitch) - David Rusell

@fa[twitter]&nbsp;[@gvilarino](https://twitter.com/gvilarino) - Guido Vilariño
