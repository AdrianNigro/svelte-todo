
@snap[north-west text-06 span-100]
### Menos código para el dev... (happiness matters)
@snapend

@snap[midpoint text-08 span-100 img-svelte-compared-loc]
![SIZE](assets/img/svelte_compared_twitter_loc.png)
@snapend

@snap[south-east text-04 span-100]
[@Rich_Harris twit](https://svelte.dev/blog/write-less-code#Death_to_boilerplate) |
[One app, four frameworks](https://dev.to/timdeschryver/my-love-letter-to-xstate-and-statecharts-287b)

@snapend
