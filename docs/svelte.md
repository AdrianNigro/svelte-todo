# svelte.md

## Speaker

Mi nombre es Sebastián Scarano, aka @opensas. Soy egresado en sistemas de la UTN, y hace varios años que me divierto (y sufro) haciendo sistemas. Soy hincha y fanático del software libre y demás libertades. Admiro a la gente que comparte su conocimiento y en cada ocasión que tengo intento hacer lo propio.

## Svelte: ni framework ni librería... ¡COMPILADOR!

...desarrollando aplicaciones web con un framework que NO es un framework

React, Vue.js, Angular, Ember.js, Meteor, Mithril, Polymer, Aurelia, Backbone.js, Preact, Inferno... ¿sigo? La batalla de los frameworks de JavaScript parece no tener fin. Cada semana aparece "el nuevo framework que va a revolucionar el desarrollo de aplicaciones web".

Ahora parece tocarle el turno a Svelte, que tiene la particularidad de tener todo el aspecto de un framework pero que se resiste a ser tratado como tal.

Y es que Svelte pretende saldar el eterno debate entre "framework" y "librería" con un nuevo enfoque, en vez de traer un nuevo runtime que se ejecuta en nuestro explorador Svelte genera JavaScript minimalista y optimizado (plain vanilla que le dicen), a partir de nuestro código.

Svelte sostiene que esto le permite no sólo generar aplicaciones super livianas sino también mejorar la experiencia del desarrollador "instrumentando" nuestro código.

En esta charla vamos a ver qué hay de cierto en todo esto, cuáles fueron las razones que llevaron a @Rich_Harris a crear Svelte, qué respuesta trae a los principales problemas que tenemos hoy para desarrollar aplicaciones, sus características principales y finalmente vamos a desarrollar interactivamente una aplicación para ver qué se siente usar Svelte.

--

## Svelte: ni framework ni librería... ¡COMPILADOR!

...desarrollando aplicaciones web con un framework que NO es un framework

(formalidades)

### speaker -

### meme polémico

### Rich Harris


### Filosofía detrás de Svelte

### Algunos números duros

### 1. Menos código... para el usuario
https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/

A RealWorld Comparison of Front-End Frameworks with Benchmarks (2019 update)

Conclusion

There is a lot of sensational things happening in this area. Svelte — The magical disappearing UI framework — really holds true to its punch line. Stencil is the new kid in this benchmark and also performs pretty well. Both are relatively new and are pushing the limits of what is possible in terms of size.

### 2. Menos código... para el dev

Write less code

https://codesandbox.io/s/lesscode-svelte-4bvtg

https://codesandbox.io/embed/lesscode-svelte-4bvtg

https://codesandbox.io/s/lesscode-sveltevsreeact-ujt7h

https://codesandbox.io/s/lesscode-sveltevsvue-sok3i

https://svelte.dev/blog/write-less-code#Death_to_boilerplate

Death to boilerplate

How? Because Svelte is a compiler, we're not bound to the peculiarities of JavaScript: we can design a component authoring experience, rather than having to fit it around the semantics of the language. Paradoxically, this results in more idiomatic code — for example using variables naturally rather than via proxies or hooks — while delivering significantly more performant apps.

--

### 3. Menos código... para ejecutar

Virtual DOM is pure overhead

https://svelte.dev/blog/virtual-dom-is-pure-overhead

https://rethinking-reactivity.surge.sh/#slide=9

### 4. Real reactivity

https://rethinking-reactivity.surge.sh/#slide=19

### 5. Entonces... ¿qué es Svelte?

### 5. Ni librería, ni framework... ni compilador!

### 5. Lenguaje

SvelteScript
https://rethinking-reactivity.surge.sh/#slide=22

The truth about Svelte

https://gist.github.com/Rich-Harris/0f910048478c2a6505d1c32185b61934

    It would extend HTML by adding JavaScript expressions in markup, directives for controlling behaviour and reacting to input, syntax for using conditions, loops and asynchronous values

    It would extend CSS by adding a scoping mechanism that kept styles from clobbering each other

    It would extend JavaScript by making reactivity a language primitive

    It would extend JavaScript by making reactivity a language primitive

How do we make reactivity a language primitive without introducing invalid syntax, or breaking the relationship with existing tooling (like TypeScript)? By hacking existing syntax:

    We instrument assignments to variables and properties
    We add $: statements, using the little-known label construct, that run whenever their inputs change value
    We tie it all together with an opinionated scheduler




#### Rethinking Reactivity
https://www.youtube.com/watch?v=AdNJ3fydeao&t=11s

https://rethinking-reactivity.surge.sh/#slide=1

https://rethinking-reactivity.surge.sh/#slide=9

#### Computer bulid me an app
https://www.youtube.com/watch?v=qqt6YxAZoOc

Frameworks exist because writing maintainable apps in vanilla JavaScript is hard. But frameworks aren’t free: downloading and parsing those extra bytes slows things up, just when your users are deciding whether to stick around.

Instead of choosing between bulky frameworks and maintainability nightmares, what if we could tell the computer ‘here are the blueprints, now you write the code’? In this talk we’ll discover a new breed of tools, such as Svelte, that let you do exactly that.










---

# Notes

repl with reactive demo: push versus assignment: https://svelte.dev/repl/8708f1443fa047089a59059c2374e253?version=3.6.7


code sand box - less code

https://codesandbox.io/s/lesscode-svelte-4bvtg

https://codesandbox.io/embed/lesscode-svelte-4bvtg

https://codesandbox.io/s/lesscode-sveltevsreeact-ujt7h

https://codesandbox.io/s/lesscode-sveltevsvue-sok3i

