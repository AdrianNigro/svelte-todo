

2018 - stateofjs

https://2018.stateofjs.com/front-end-frameworks/conclusion/

--


https://logrocket.com/blog/history-of-frontend-frameworks/

https://dev.to/_adam_barker/the-super-brief-history-of-javascript-frameworks-for-those-somewhat-interested-3m82

https://www.youtube.com/watch?v=Kzeog8yTFaE

https://pbs.twimg.com/media/CDx_joRXIAAuxZf.png

https://raw.githubusercontent.com/mraible/history-of-web-frameworks-timeline/master/history-of-web-frameworks-timeline.png

https://speakerdeck.com/initfabian/a-brief-history-of-front-end-frameworks

https://speakerdeck.com/xbill82/the-frontend-is-a-fullstack

1991 - The first HTML specification was made public in late 1991 by Tim Berners-Lee. It only supported text at the time and consisted of just 18 tags.

1996 - HTML 4.0 was published which was the first specification to support CSS.

Internet Explorer 3 became the first commercial browser to support CSS.

1995 - a new browser scripting language called Mocha was created by Brendan Eich in just 10 days. It got renamed to LiveScript. A few months later it got renamed again to JavaScript as we now know it.

check this: https://speakerdeck.com/initfabian/a-brief-history-of-front-end-frameworks?slide=5

https://github.com/mraible/history-of-web-frameworks-timeline

1999 - Dynamic HTML

In the early 2000’s, JavaScript was largely in its infancy, and was mostly used to perform basic changes to a page.

2000

Internet Explorer dominated the web browser landscape (I know, weird right?) and since 1999, included a wrapper around a library Microsoft had developed for their own e-mail product, Outlook.

XMLHttpRequest object eventually became standardized across browsers

XMLHttpRequest allowed requests to be sent to a server, and a response processed, without having to refresh the browser or perform a full round trip.

2004 -

1996 - Internet Explorer dominated the web browser landscape (I know, weird right?) and since 1999, included a wrapper around a library Microsoft had developed for their own e-mail product, Outlook.

XMLHttpRequest object eventually became standardized across browsers

In 2004, Google implemented a standardized version of AJAX technology on their Gmail and Google Maps products.

In 2005, Google astounded the world with the launch of Google Maps. Drag the screen and, like magic, little tiles would pop into view, one by one.

https://hackernoon.com/the-backendification-of-frontend-development-62f218a773d4
1990 - 1995 : surgen HTML, CSS and JavaScript, los pilares de la programación web

1996 - 2004 : Las páginas son mayormente estáticas, cada interacción supone cargar de vuelta la página.

2005 jQuery se instala como la librería dominante en desarrollo web (provee una api más amigable y salva las inconsistencias entre los browsers)

2006 Google presenta Google Maps, la primera aplicación AJAX, usando una olvidada tecnología de 1999 (XMLHttpRequest)

--

2010 Backbone: la primera librería orientada a crear Single Page Applications (SPA)

2010 AngularJS: el primer framework que provee una arquitectura completa para el desarrollo front-end. Trajo data binding bi-directional, inyección de dependencias y la posibilidad de crear componente reutilizables

2013 React: basado en la idea de componentes y el Virtual DOM, tomando elementos de la programación funcional (state driven apps, enfoque declarativo, sin mutationces)

2014 Vue: en algunos aspectos es un punto intermedio entre Angular y React, con una curva de parendizaje más liviana.


Problemas del desarrollo de web apps de hoy:

- Complejidad: MUY difícil (demasiadas cosas para aprender)

- Tamaño: MUY pesado (demasiados kb para descargar, interpretar y ejecutar)

- Verborrágico: MUCHO código (demasiado código para escribir y mantener)



2018 - stateofjs

https://2018.stateofjs.com/front-end-frameworks/conclusion/

Finally, keep an eye out for Svelte. By using a radical new approach to front-end frameworks, it's managed to generate quite a lot of interest and was by far the most mentioned option in our "Other Tools" category.

---


For many, this was the first time they would experience the power of AJAX in action. It was the “killer app” that proved client-side applications had the potential to deliver massively superior user experiences. This was a major step forward on our journey towards the modern Single Page Application (SPA) of today.



2006

jQuery aimed to solve many of the issues developers faced with subtle differences in web browser implementations

It helped developers write JavaScript code without the worry of browser compatibility. It contained tons of useful functions that made it easy to make any website interactive.

2010 - Backbone

In 2010, Jeremy Ashkenas released Backbone, the first framework aimed at creating single page applications. Jeremy had seen how messy a huge jQuery application could become and wanted a cleaner approach to remedy the thousands of selectors and interwoven event handlers

It was the first attempt to make a 100% javascript application without any annoying reloads. The idea of Models and Collections behind Backbone made web application development more structured and created with well documented and clear boundaries understandable  for many developers.

2010 - AngularJS - Google’s, the first framework that provided a complete architecture for front-end application development.

bi-directional data binding and provided a way to bind a model’s data to HTML markup and have changes update in real-time. Developers called this ‘automagic’.

Angular also supported dependency injection and the ability to create reusable components.

 It came with strong opinions on how to structure your projects, removing a lot of the error-prone paperwork that we now associate with JQuery.


2013 - React

Angular releases a new version, Unfortunately, this new version was completely incompatible with AngularJS. There was no migration path that was provided.

In May 2013 at a JavaScript Conference in the US, a new game-changer library called React was introduced. It was created by Jordan Walke, a software engineer working for Facebook. The audience was amazed by many of its innovative features such as the Virtual DOM, the one-way data flow and the Flux pattern.

 React which was based around the idea compositional components and a lightning fast Virtual DOM

(functional inspiration)

Very declarative
Discourages mutation
Virtual DOM
Redux - unidirectional dataflow

At the core of the style of programming that characterises this combination is the embrace of immutable data. Typically, a React component is merely a function that describes what, given the state of your application

2014 - Vue

Unlike React, which is very flexible, or Angular which is very opinionated, Vue tries to pick the middle ground.


---

1990 - 1995 : HTML, CSS and JavaScript are invented

1996 - 1999 : Standardization efforts begin. Browser compliance is terrible. Browser wars ignite.

2000 - 2004 : CSS frameworks begin to emerge. jQuery is born. Frontend package management.

In 2005, Google astounded the world with the launch of Google Maps

Drag the screen and, like magic, little tiles would pop into view, one by one.


2005 - 2009 : W3C specification compliance is met. Chrome browser takes the lead. Responsive designs and frameworks are introduced.

2010 - 2015: JavaScript Frameworks are born i.e. Backbone, Ember, AngularJS, React, Angular, Vue. HTML5 is announced.

2016 - 2018: GraphQL emerges. Native HTML, CSS & JavaScript become more powerful. New platforms built on-top existing JavaScript frameworks emerge: StoryBook, Motion UI, Gatsby, Next.js.


2018 - stateofjs

https://2018.stateofjs.com/front-end-frameworks/conclusion/

Finally, keep an eye out for Svelte. By using a radical new approach to front-end frameworks, it's managed to generate quite a lot of interest and was by far the most mentioned option in our "Other Tools" category.
