https://github.com/meetupjs-ar/charlas/issues/new (2019/07/20)

https://meetupjs.com.ar/


### Título
Svelte: Ni librería ni framework... COMPILADOR!!!

### Descripción

React, Vue.js, Angular, Ember.js, Meteor, Mithril, Polymer, Aurelia, Backbone.js, Preact, Inferno... ¿sigo? La batalla de los frameworks de JavaScript parece no tener fin. Cada semana aparece "el nuevo framework que va a revolucionar el desarrollo de aplicaciones web".

Ahora parece tocarle el turno a Svelte, que tiene la particularidad de tener todo el aspecto de un framework pero que se resiste a ser tratado como tal.

Svelte pretende saldar el eterno debate entre "framework" y "librería" con un nuevo enfoque, en vez de traer un nuevo runtime que se ejecuta en nuestro explorador Svelte genera JavaScript minimalista y optimizado (plain vanilla js que le dicen), a partir de nuestro código.

Svelte sostiene que esto le permite no sólo generar aplicaciones super livianas sino también mejorar la experiencia del desarrollador "instrumentando" nuestro código.

En esta charla veremos qué hay de cierto en todo esto, cuáles fueron las razones que llevaron a @Rich_Harris a crear Svelte y qué respuesta trae a los principales problemas que tenemos hoy para desarrollar aplicaciones.

Para ver cómo se traduce todo esto en términos prácticos, vamos a desarrollar interactivamente una aplicación completa, en la cual veremos cómo iniciar una aplicación Svelte, cómo crear nuestros propios componentes, qué opciones tenemos para que se comuniquen entre sí, cómo gestionar el estado de nuestra UI, cómo deployar nuestras aplicaciónes y, finalmente, qué se siente desarollar aplicaciones web con Svelte.

### Tus datos

* **Bio**:
Mi nombre es Sebastián Scarano. Soy Ingeniero en Sistemas de la UTN y hace años que me dedico a desarrollar aplicaciones. Soy defensor del software libre y demás libertades. Me gusta aprender de los demás y compartir mis experiencias. Participo de Nardoz, HackHacers y demás comunidades dateras y nerd.

* **Foto**:
https://gitpitch.com/pitchme/cdn/gitlab/opensas/svelte-todo/master/F9A4D9CDC0107424CBF0D27462E4BE0D7C761B3D251F33E8AD3AA76446888AA30D2F05E879CA82345507E78A9A5C586D0D3041187F42BE34299AC353C6AA79CEA475A606AC6CA7D1BC6619EF19CF55B54907F2197069F9DF721F56C86B0A17DC/assets/img/opensas_avatar.jpg

* **Usuario de Slack**:
opensas (Sebastián Scarano)

* **Usuario de Twitter**: @opensas

* **Otros usuarios**: Si no tenés Twitter dejanos acá otros links donde te gustaría que te mencionemos en los tweets y otras publicaciones.

* **Pronombre**:
opensas

### Recursos

* **Slides de la presentación**: https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/

* **Repositorio de la charla**: https://gitlab.com/opensas/svelte-todo

### Extra

* **¿Tenés la charla preparada?**: Sí

* **¿A partir de qué mes la podrías dar?:** Me voy de vacaciones del 27/7 y vuelvo el 12/8

> Avisanos si necesitás algo especial para poder dar la charla así lo podemos planificar con tiempo.

WIFI!!!! y proyector
